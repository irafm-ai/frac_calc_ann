# Fractional activation functions

This repository contains source code of fractional activation functions including:
- Fractional sigmoid
- Fractional Mish
- Fractional GELU
- Fractional adaptive linear unit (FALU)
  
To train ResNet{20, 32, 44, 56, 110} on CIFAR-10, use following command:

`CUDA_VISIBLE_DEVICES=X, CUBLAS_WORKSPACE_CONFIG=:4096:8 python train_cifar.py --model=resnet20 --activation_fc=frac_sig --sigma_it=2 --ds_ver=10 --deterministic --wandb=online --lr=0.1 --fp16 --grad_clip`. 

Set `CUDA_VISIBLE_DEVICES` to the id of your GPU(s). `CUBLAS_WORKSPACE_CONFIG=:4096:8` is for deterministic runs. Corresponding `sigma_it` values are in `exe_scripts/cifar_resnet_frac.sh`. This script will run training of all fractional activation functions on CIFAR-10 and should replicate our results from the paper.

The `exe_scripts` folder contains bash scripts for reproducing the experiments and ablation studies in the accompanying paper.

# DevContainer

This repository contains a devcontainer for easy reproduction. You must adjust the paths to the datasets in `.devcontainer/devcontainer.json`. In the case you do not want mount folders from the local system, just comment out the mount lines in the json and PyTorch will download those datasets to container. Be warmed, that this will break ImageNet as it must be downloaded manually.

# Weight and Biases

On the [following link](https://api.wandb.ai/links/osu/n7st8s2l) you can find all the experiments.