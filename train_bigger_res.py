import argparse
import os
import sys
from functools import partial
from typing import Tuple

import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torchvision
import torchvision.transforms as transforms
from torch import nn
from torch.utils.data import Subset
from torcheval.metrics import MulticlassAccuracy
from tqdm import tqdm

import fractional_derivative as fda
import wandb
from fractional_derivative.activation_functions import BaseFracAct
from fractional_derivative.caltech256 import RGBCaltech256
from fractional_derivative.transforms import Resize

logger = fda.get_logger(os.path.basename(__file__))


def log_file_wandb(path: str, name: str):
    artifact = wandb.Artifact(name, 'module')
    artifact.add_file(path)
    wandb.log_artifact(artifact)


def get_augmentations(
    train_res: int,
    test_res: int,
    mean: Tuple[float, float, float],
    std: Tuple[float, float, float],
) -> transforms.Compose:
    transform_train = transforms.Compose(
        [
            transforms.RandomResizedCrop(train_res),
            transforms.RandomHorizontalFlip(),
            transforms.ColorJitter(0.3, 0.3, 0.3),
            transforms.ToTensor(),
            transforms.Normalize(mean, std),
        ]
    )

    transform_test = transforms.Compose(
        [
            Resize(int((256 / 224) * test_res)),  # to maintain same ratio w.r.t. 224 images
            transforms.CenterCrop(test_res),
            transforms.ToTensor(),
            transforms.Normalize(mean, std),
        ]
    )

    return transform_train, transform_test


def train(criterion, dataloader, num_classes, device, optimizer, net, pool, grad_clip, fp16):
    acc = MulticlassAccuracy(num_classes=num_classes, device=device)
    sum_loss = torch.tensor(0.0, device=device)
    scaler = torch.cuda.amp.grad_scaler.GradScaler()

    net.train()
    for i, (inputs, targets) in enumerate(dataloader):
        inputs, targets = inputs.to(device), targets.to(device)
        optimizer.zero_grad()
        with torch.autocast(device_type=device, dtype=torch.float16, enabled=fp16):
            outputs = net(inputs)
            loss = criterion(outputs, targets)
            if torch.any(torch.isnan(loss)):
                sys.exit('NaN occurred in training loss function. Terminating ...')
        scaler.scale(loss).backward()
        if grad_clip > 0:
            scaler.unscale_(optimizer)
            torch.nn.utils.clip_grad.clip_grad_norm_(net.parameters(), grad_clip, foreach=True)

        scaler.step(optimizer)
        scaler.update()

        if pool:
            try:
                for act_fc in pool:
                    act_fc.clip_params(
                        _min=fda.activation_functions.MIN_FRAC_ORDER,
                        _max=fda.activation_functions.MAX_FRAC_ORDER,
                    )
            except AttributeError:
                pass

        sum_loss += loss
        acc.update(outputs, targets)

    return sum_loss.item() / len(dataloader), round(100.0 * acc.compute().item(), 2)


@torch.no_grad()
def test(criterion, dataloader, num_classes, device, net):
    acc = MulticlassAccuracy(num_classes=num_classes, device=device)
    net.eval()
    sum_loss = torch.tensor(0.0, device=device)

    for inputs, targets in dataloader:
        inputs, targets = inputs.to(device), targets.to(device)
        outputs = net(inputs)
        loss = criterion(outputs, targets)

        sum_loss += loss
        acc.update(outputs, targets)

    return sum_loss.item() / len(dataloader), round(100.0 * acc.compute().item(), 2)


def main():
    SEED = 42

    parser = argparse.ArgumentParser(
        'Training script for other dataset than CIFAR-10. For B0-B7 train/test resolution see FIXING THE TRAIN-TEST RESOLUTION DISCREPANCY: FIXEFFICIENTNET, table 2.'
    )
    # fmt: off
    # model
    parser.add_argument('--model', default='effnetb0', type=str, help='Architecture. Defaults to effnetb0',)
    parser.add_argument('--activation_fc', default='relu', type=str, help='Activation function. Defaults to relu.',
                        choices=['relu', 'prelu', 'sig', 'frac_sig', 'gelu', 'frac_gelu', 'mish', 'frac_mish', 'falu', 'swish'])
    parser.add_argument('--sigma_it', default=3, type=int, help='Number of sigma (summation) iteration. Defaults to 3.')

    # dataset
    parser.add_argument('--ds', default='imagenet', type=str, help='Dataset name', choices=['imagenet', 'food101', 'caltech256'])

    # training
    parser.add_argument('--train_res', default=224, type=int)
    parser.add_argument('--test_res', default=320, type=int)
    parser.add_argument('--lr', default=0.025, type=float, help='Base lr. IT IS NOT final lr, see this script for actual lr computation.')
    parser.add_argument('--epochs', default=30, type=int)
    parser.add_argument('--batch_size', default=64, type=int)
    parser.add_argument('--label_smoothing', default=0.1, type=float)
    parser.add_argument('--train_portion', default=1.0, type=float, help='Portion of train dataset used for training. Samples are selected randomly from dataset.')
    parser.add_argument('--deterministic', action='store_true', help='Switch for using seeded run and using PyTorch deterministic algorithms.')
    parser.add_argument('--grad_clip', default=10, type=float, help='Max norm argument of gradient clipping norm.')
    parser.add_argument('--fp16', action='store_true', help='Enable floating point 16 bit ops.')

    # misc
    parser.add_argument('--wandb', default='disabled', type=str, help='Enables logging to w&b.')
    parser.add_argument('--group', default=None, type=str, help='Group argument of w&b.')

    # fmt: on
    args = parser.parse_args()
    logger.info(f'Script called with following arguments {vars(args)}')

    g = None
    worker_seed = None
    if args.deterministic:
        logger.info('Using seed and deterministic algorithms')
        fda.set_seed(seed=SEED)
        g = torch.Generator()
        g.manual_seed(0)
        worker_seed = fda.seed_worker

    wandb.login()
    wandb.init(
        project='fractional_derivative_activation_functions',
        entity='osu',
        mode=args.wandb,
        config=args,
    )

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    logger.info(f'Executing on device type {device}')

    if device == 'cuda':
        torch.set_float32_matmul_precision('high')
        if args.deterministic:
            cudnn.deterministic = True
            cudnn.benchmark = False
        else:
            cudnn.benchmark = True

    # https://github.com/facebookresearch/FixRes/blob/c9be6acc7a6b32f896e62c28a97c20c2348327d3/imnet_finetune/transforms.py#L47
    mean, std = [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]

    transform_train, transform_test = get_augmentations(args.train_res, args.test_res, mean, std)

    if args.ds == 'imagenet':
        trainset = torchvision.datasets.ImageNet(
            root='data/imagenet/2012/images',
            split='train',
            transform=transform_train,
        )
        testset = torchvision.datasets.ImageNet(
            root='data/imagenet/2012/images',
            split='val',
            transform=transform_test,
        )
        num_classes = len(trainset.classes)
    elif args.ds == 'food101':
        trainset = torchvision.datasets.Food101(
            root='data/food101/torch',
            split='train',
            transform=transform_train,
            download=True,
        )
        testset = torchvision.datasets.Food101(
            root='data/food101/torch',
            split='test',
            transform=transform_test,
        )
        num_classes = len(trainset.classes)
    elif args.ds == 'caltech256':
        trainset = RGBCaltech256(
            root='data/caltech256/torch',
            transform=transform_train,
            download=False,
        )
        num_classes = len(trainset.categories)

        # Manually splitting the dataset so that training dataset contains exactly 60 images per class.
        # Same sampling as in: Weakly Supervised Complementary Parts Models for Fine-Grained Image Classification from the Bottom Up
        idx = np.array([i for i in range(len(trainset))])
        y = np.array(trainset.y)
        classes = np.unique(y)
        train_idx = np.array([], dtype=np.int32)
        test_idx = np.array([], dtype=np.int32)
        for c in classes:
            # idx of class c
            c_idx = idx[y == c]
            # select train idx from class idx
            c_train = np.random.choice(c_idx, 60, replace=False)
            # add it to train idx
            train_idx = np.concatenate((train_idx, c_train))
            # get inverse of class train idx, i.e. idx that are not in c_train
            c_test = c_idx[np.in1d(c_idx, c_train, invert=True)]
            # add them to test idx
            test_idx = np.concatenate((test_idx, c_test))

        trainset = Subset(trainset, train_idx)

        testset = RGBCaltech256(
            root='data/caltech256/torch',
            transform=transform_test,
            download=False,
        )
        testset = Subset(testset, test_idx)
        # assert that train and test set are disjunctive
        assert np.all(np.in1d(train_idx, test_idx, invert=True))
    else:
        raise ValueError(f'unknown dataset {args.ds}')

    logger.info(f'training dataset contains {num_classes} classes and {len(trainset)} samples')

    if args.train_portion < 1:
        logger.info(f'train portion {args.train_portion} < 1')
        idx = np.random.randint(0, len(trainset), size=int(args.train_portion * len(trainset)))
        trainset = Subset(trainset, idx)

    trainloader = torch.utils.data.DataLoader(
        trainset,
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=8,
        pin_memory=True,
        generator=g,
        worker_init_fn=worker_seed,
    )

    testloader = torch.utils.data.DataLoader(
        testset,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=8,
        pin_memory=True,
        generator=g,
        worker_init_fn=worker_seed,
    )

    # NOTE: Two function that maps string - model, for simplicity of loging
    activation_fc = fda.parser.get_activation_fc(name=args.activation_fc)

    pool = None
    if hasattr(activation_fc, 'pool'):
        pool = getattr(activation_fc, 'pool')

    if issubclass(activation_fc, BaseFracAct):
        assert args.sigma_it > 0
        h = 1.0 / max(1.0, args.sigma_it - 1)
        activation_fc = partial(activation_fc, iter=args.sigma_it, h=h)
        logger.info(f'h is set to {h}')
    else:
        logger.info('activation function is not fractional')

    net = fda.parser.get_model(
        args.model,
        activation_fc=activation_fc,
        num_classes=num_classes,
    )

    params = sum(param.numel() for param in net.parameters())
    logger.info(f'{args.model} number of parameters: {params}')
    net = net.to(device)

    criterion = nn.CrossEntropyLoss(label_smoothing=args.label_smoothing).to(device)
    params = fda.disable_weight_decay_for_frac_af(net.named_parameters(), weight_decay=1e-4)
    optimizer = torch.optim.SGD(
        params,
        # lr is for batch 64. For smaller batches we linearly scale down lr
        lr=args.batch_size / 64.0 * args.lr,
        momentum=0.9,
    )
    # Dirty solution: this is for eva02 only! https://arxiv.org/pdf/2303.11331
    # optimizer = torch.optim.AdamW(
    #     params,
    #     # lr is for batch 64. For smaller batches we linearly scale down lr
    #     lr=args.batch_size / 64.0 * args.lr,
    # )
    # First 2 epochs are for warmup
    lrs = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, 4, 2)
    warmup = torch.optim.lr_scheduler.LinearLR(optimizer, 1e-6, total_iters=2)
    scheduler = torch.optim.lr_scheduler.SequentialLR(optimizer, [warmup, lrs], milestones=[2])

    # log python module/file containing activation functions
    log_file_wandb('fractional_derivative/activation_functions.py', 'activation_functions_module')
    # log this training script
    log_file_wandb(__file__, 'train_script')

    with tqdm(range(args.epochs)) as it:
        best_test_acc = 0.0
        for ep in it:

            train_loss, train_acc = train(
                criterion, trainloader, num_classes, device, optimizer, net, pool, args.grad_clip, args.fp16
            )
            test_loss, test_acc = test(criterion, testloader, num_classes, device, net)

            if test_acc > best_test_acc:
                best_test_acc = test_acc
            it.set_postfix_str(f'test loss {test_loss:.4f} best test acc {best_test_acc}%')

            log = {
                'train_loss': train_loss,
                'train_acc': train_acc,
                'test_loss': test_loss,
                'test acc': test_acc,
                'best test acc': best_test_acc,
                'lr': scheduler.get_last_lr()[0],
            }

            if pool:
                # log trainable parameter of fractional activation functions to wandb
                degrees_log = {}
                # fractional activation function classes have list of all instances of given class in _pool (list)
                for i, fc in enumerate(pool):
                    degrees_log[f'act_fc_{i}_frac_order'] = fc.frac_order.detach().cpu().numpy()
                log.update(degrees_log)

            wandb.log(log)
            warmup.step()
            scheduler.step()


if __name__ == '__main__':
    main()
