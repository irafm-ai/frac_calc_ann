# runs ablation study, searching for the best number of sigma iterations
iters=( 1 2 3 4 )
for iter in "${iters[@]}"
do
    acts=( frac_mish frac_gelu frac_sig )
    for act in "${acts[@]}"
    do
        # dont forget to change cuda device
        CUDA_VISIBLE_DEVICES=0, CUBLAS_WORKSPACE_CONFIG=:4096:8 python train_bigger_res.py --model=effnetb0 --ds=imagenet --activation_fc=$act --sigma_it=$iter --wandb=online --train_portion=0.1 --epochs=30 --batch_size=64 --train_res=224 --test_res=320 --deterministic --fp16
    done
done
