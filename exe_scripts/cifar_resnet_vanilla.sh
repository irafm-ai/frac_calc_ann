archs=( resnet20 resnet32 resnet44 resnet56 resnet110 )
for arch in "${archs[@]}"
do
    act_fcs=( relu prelu gelu sig mish falu )
    for act_fc in "${act_fcs[@]}"
    do
        CUDA_VISIBLE_DEVICES=0, CUBLAS_WORKSPACE_CONFIG=:4096:8 python train_cifar.py --model=$arch --activation_fc=$act_fc --ds_ver=10  --deterministic --wandb=online --lr=0.1 --grad_clip --fp16
    done
done
