archs=( resnet20 resnet32 resnet44 resnet56 resnet110 )
for arch in "${archs[@]}"
do
    # best params found with ab study
    iters=( 2 1 2 )
    act_fcs=( frac_sig frac_gelu frac_mish )
    for (( i=0; i<${#iters[@]}; i++ ));
    do
        # dont forget to change cuda device
        CUDA_VISIBLE_DEVICES=0, CUBLAS_WORKSPACE_CONFIG=:4096:8 python train_cifar.py --model=$arch --activation_fc=${act_fcs[$i]} --sigma_it=${iters[$i]} --ds_ver=10 --deterministic --wandb=online --lr=0.1 --fp16 --grad_clip
    done
done
