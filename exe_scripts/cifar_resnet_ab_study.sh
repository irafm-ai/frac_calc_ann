# runs ablation study, searching for the best number of sigma iterations
lrs=( 0.1 )
for lr in "${lrs[@]}"
    do
    iters=( 1 2 3 4 5 )
    for iter in "${iters[@]}"
    do
        acts=( frac_mish frac_gelu frac_sig )
        for act in "${acts[@]}"
        do
            # dont forget to change cuda device
            CUDA_VISIBLE_DEVICES=0, CUBLAS_WORKSPACE_CONFIG=:4096:8 python train_cifar.py --model=resnet20 --activation_fc=$act --sigma_it=$iter --lr=$lr --ds_ver=10 --optimizer=adam --deterministic --wandb=online --grad_clip --train_portion=0.5 --epochs=100 --fp16
        done
    done
done
