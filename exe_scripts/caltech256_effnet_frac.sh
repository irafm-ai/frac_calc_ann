archs=( effnetb0 )
for arch in "${archs[@]}"
do
    iters=(2 1 2)
    act_fcs=( frac_sig frac_gelu frac_mish )
    for (( i=0; i<${#iters[@]}; i++ ));
    do
        # dont forget to change cuda device
        CUDA_VISIBLE_DEVICES=0, CUBLAS_WORKSPACE_CONFIG=:4096:8 python train_bigger_res.py --model=$arch --ds=caltech256 --activation_fc=${act_fcs[$i]} --sigma_it=${iters[$i]} --deterministic --wandb=online --epochs=256 --batch_size=64 --train_res=224 --test_res=320 --fp16
    done
done
