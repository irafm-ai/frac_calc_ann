archs=( mobilenetv4_conv_small mobilenetv4_conv_medium)
for arch in "${archs[@]}"
do
    act_fcs=( relu prelu gelu sig mish swish falu )
    for act_fc in "${act_fcs[@]}"
    do
        # dont forget to change cuda device
        CUDA_VISIBLE_DEVICES=0, CUBLAS_WORKSPACE_CONFIG=:4096:8 python train_bigger_res.py --model=$arch --ds=caltech256 --activation_fc=$act_fc --deterministic --wandb=online --epochs=256 --batch_size=64 --train_res=224 --test_res=320 --fp16
    done
done
