import argparse
from functools import partial
import os
import sys
from typing import Tuple

import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torchvision
import torchvision.transforms as transforms
from torch import nn
from torch.optim.lr_scheduler import LinearLR, MultiStepLR
from torch.utils.data import Subset
from tqdm import tqdm
from torcheval.metrics import MulticlassAccuracy
import pytorch_optimizer as pto

import fractional_derivative as fda
import wandb

from fractional_derivative.activation_functions import BaseFracAct

logger = fda.get_logger(os.path.basename(__file__))


def log_file_wandb(path: str, name: str):
    artifact = wandb.Artifact(name, 'module')
    artifact.add_file(path)
    wandb.log_artifact(artifact)


def get_augmentations(
    augmentations: str,
    mean: Tuple[float, float, float],
    std: Tuple[float, float, float],
) -> transforms.Compose:
    transform_test = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Normalize(mean, std),
        ]
    )

    if augmentations == 'default':
        transform_train = transforms.Compose(
            [
                transforms.RandomCrop(32, padding=4),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                transforms.Normalize(mean, std),
            ]
        )
    elif augmentations == 'trivial_wide':
        transform_train = transforms.Compose(
            [
                transforms.TrivialAugmentWide(),
                transforms.ToTensor(),
                transforms.Normalize(mean, std),
            ]
        )
    elif augmentations == 'augmix':
        transform_train = transforms.Compose(
            [
                transforms.AugMix(),
                transforms.ToTensor(),
                transforms.Normalize(mean, std),
            ]
        )
    else:
        raise ValueError(augmentations)

    return transform_train, transform_test


def get_optimizer(name: str, lr: float, net: torch.nn.Module):
    if name == 'sgd':
        params = fda.disable_weight_decay_for_frac_af(net.named_parameters(), 5e-4)
        optimizer = torch.optim.SGD(
            params,
            lr=lr,
            momentum=0.9,
            # weight_decay=5e-4
        )
    elif name == 'adam':
        params = fda.disable_weight_decay_for_frac_af(net.named_parameters(), 5e-4)
        optimizer = torch.optim.Adam(
            params,
            lr=lr,
            # weight_decay=5e-4
        )
    elif name == 'lookahead':
        params = fda.disable_weight_decay_for_frac_af(net.named_parameters(), 5e-4)
        optimizer = torch.optim.SGD(
            params,
            lr=lr,
            momentum=0.9,
            # weight_decay=5e-4
        )
        optimizer = pto.Lookahead(optimizer)
    else:
        raise ValueError(f'Unknown optimizer {name}')

    return optimizer


def train(criterion, dataloader, device, optimizer, net, pool, grad_clip, fp16):
    acc = MulticlassAccuracy(num_classes=10, device=device)
    sum_loss = torch.tensor(0.0, device=device)
    scaler = torch.cuda.amp.grad_scaler.GradScaler()

    net.train()
    for i, (inputs, targets) in enumerate(dataloader):
        inputs, targets = inputs.to(device), targets.to(device)
        optimizer.zero_grad()
        with torch.autocast(device_type=device, dtype=torch.float16, enabled=fp16):
            outputs = net(inputs)
            loss = criterion(outputs, targets)
            if torch.any(torch.isnan(loss)):
                sys.exit('NaN occurred in training loss function. Terminating ...')
        scaler.scale(loss).backward()
        if grad_clip:
            scaler.unscale_(optimizer)
            torch.nn.utils.clip_grad.clip_grad_norm_(net.parameters(), 10.0, foreach=True)

        scaler.step(optimizer)
        scaler.update()

        if pool:
            try:
                for act_fc in pool:
                    act_fc.clip_params(
                        _min=fda.activation_functions.MIN_FRAC_ORDER,
                        _max=fda.activation_functions.MAX_FRAC_ORDER,
                    )
            except AttributeError:
                pass

        sum_loss += loss
        acc.update(outputs, targets)

    return sum_loss.item() / len(dataloader), round(100.0 * acc.compute().item(), 2)


@torch.no_grad()
def test(criterion, dataloader, device, net):
    acc = MulticlassAccuracy(num_classes=10, device=device)
    net.eval()
    sum_loss = torch.tensor(0.0, device=device)

    for inputs, targets in dataloader:
        inputs, targets = inputs.to(device), targets.to(device)
        outputs = net(inputs)
        loss = criterion(outputs, targets)

        sum_loss += loss
        acc.update(outputs, targets)

    return sum_loss.item() / len(dataloader), round(100.0 * acc.compute().item(), 2)


def main():
    SEED = 42

    parser = argparse.ArgumentParser()
    # fmt: off
    # model
    parser.add_argument('--model', default='resnet20', type=str, help='Architecture. Defaults to resnet20',
                        choices=['resnet20', 'resnet32', 'resnet44', 'resnet56', 'resnet110'])
    parser.add_argument('--activation_fc', default='relu', type=str, help='Activation function. Defaults to relu.',
                        choices=['relu', 'prelu', 'sig', 'frac_sig', 'gelu', 'frac_gelu', 'mish', 'frac_mish', 'falu'])
    parser.add_argument('--sigma_it', default=3, type=int, help='Number of sigma (summation) iteration. Defaults to 3.')

    # dataset
    parser.add_argument('--ds_ver', default='10', type=str, help='Version of cifar, either 10 or 100.',
                        choices=['10', '100'])

    # training
    parser.add_argument('--optimizer', default='sgd', type=str, choices=['sgd', 'adam', 'lookahead'])
    parser.add_argument('--lr', default=0.1, type=float, help='learning rate.')
    parser.add_argument('--epochs', default=200, type=int)
    parser.add_argument('--batch_size', default=128, type=int)
    parser.add_argument('--label_smoothing', default=0.1, type=float)
    parser.add_argument('--train_portion', default=1.0, type=float, help='Portion of train dataset used for training. Samples are selected randomly from dataset.')
    parser.add_argument('--deterministic', action='store_true', help='Switch for using seeded run and using PyTorch deterministic algorithms.')
    parser.add_argument('--grad_clip', action='store_true')
    parser.add_argument('--augmentations', default='default',
                        choices=['default', 'trivial_wide', 'augmix'])
    parser.add_argument('--fp16', action='store_true', help='Enable floating point 16 bit ops.')

    # misc
    parser.add_argument('--wandb', default='disabled', type=str, help='Enables logging to w&b.')
    parser.add_argument('--group', default=None, type=str, help='Group argument of w&b.')

    # fmt: on
    args = parser.parse_args()
    logger.info(f'Script called with following arguments {vars(args)}')

    g = None
    worker_seed = None
    if args.deterministic:
        logger.info('Using seed and deterministic algorithms')
        fda.set_seed(seed=SEED)
        g = torch.Generator()
        g.manual_seed(0)
        worker_seed = fda.seed_worker

    wandb.login()
    wandb.init(
        project='fractional_derivative_activation_functions',
        entity='osu',
        mode=args.wandb,
        config=args,
    )

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    logger.info(f'Executing on device type {device}')

    if device == 'cuda':
        torch.set_float32_matmul_precision('high')
        if args.deterministic:
            cudnn.deterministic = True
            cudnn.benchmark = False
        else:
            cudnn.benchmark = True

    # https://github.com/facebookarchive/fb.resnet.torch/issues/180
    mean = [0.4913997551666284, 0.48215855929893703, 0.4465309133731618]
    std = [0.24703225141799082, 0.24348516474564, 0.26158783926049628]

    transform_train, transform_test = get_augmentations(args.augmentations, mean, std)

    if args.ds_ver == '10':
        ds = torchvision.datasets.CIFAR10
    elif args.ds_ver == '100':
        ds = torchvision.datasets.CIFAR100
    else:
        raise ValueError()

    trainset = ds(
        root=f'data/cifar{args.ds_ver}_torch',
        train=True,
        download=True,
        transform=transform_train,
    )
    num_classes = len(trainset.classes)

    if args.train_portion < 1:
        idx = np.random.randint(0, len(trainset), size=int(args.train_portion * len(trainset)))
        trainset = Subset(trainset, idx)

    trainloader = torch.utils.data.DataLoader(
        trainset,
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=8,
        pin_memory=True,
        generator=g,
        worker_init_fn=worker_seed,
    )

    testset = ds(
        root=f'data/cifar{args.ds_ver}_torch',
        train=False,
        download=True,
        transform=transform_test,
    )
    testloader = torch.utils.data.DataLoader(
        testset,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=8,
        pin_memory=True,
        generator=g,
        worker_init_fn=worker_seed,
    )

    # NOTE: Two function that maps string - model, for simplicity of loging
    activation_fc = fda.parser.get_activation_fc(name=args.activation_fc)

    pool = None
    if hasattr(activation_fc, 'pool'):
        pool = getattr(activation_fc, 'pool')

    if issubclass(activation_fc, BaseFracAct):
        assert args.sigma_it > 0
        h = 1.0 / max(1.0, args.sigma_it - 1)
        activation_fc = partial(activation_fc, iter=args.sigma_it, h=h)
        logger.info(f'h is set to {h}')
    else:
        logger.info('activation function is not fractional')

    net = fda.parser.get_model(
        args.model,
        activation_fc=activation_fc,
        num_classes=num_classes,
    )

    params = sum(param.numel() for param in net.parameters())
    logger.info(f'{args.model} number of parameters: {params}')
    net = net.to(device)

    criterion = nn.CrossEntropyLoss(label_smoothing=args.label_smoothing).to(device)
    optimizer = get_optimizer(args.optimizer, args.lr, net)
    warmup = LinearLR(optimizer, 0.0002, total_iters=5)
    scheduler = MultiStepLR(
        optimizer,
        milestones=[
            int(0.3 * args.epochs),  # default 60
            int(0.6 * args.epochs),  # default 120
            int(0.8 * args.epochs),  # default 160
        ],
        gamma=0.2,
    )

    log_file_wandb('fractional_derivative/activation_functions.py', 'activation_functions_module')
    log_file_wandb(__file__, 'train_script')

    with tqdm(range(args.epochs)) as it:
        best_test_acc = 0.0
        for ep in it:
            train_loss, train_acc = train(
                criterion, trainloader, device, optimizer, net, pool, args.grad_clip, args.fp16
            )
            test_loss, test_acc = test(criterion, testloader, device, net)

            if test_acc > best_test_acc:
                best_test_acc = test_acc
            it.set_postfix_str(f'test loss {test_loss:.4f} best test acc {best_test_acc}%')

            log = {
                'train_loss': train_loss,
                'train_acc': train_acc,
                'test_loss': test_loss,
                'test acc': test_acc,
                'best test acc': best_test_acc,
                'lr': scheduler.get_last_lr()[0],
            }

            if pool:
                # log trainable parameter of fractional activation functions to wandb
                degrees_log = {}
                # fractional activation function classes have list of all instances of given class in _pool (list)
                for i, fc in enumerate(pool):
                    degrees_log[f'act_fc_{i}_frac_order'] = fc.frac_order.detach().cpu().numpy()
                log.update(degrees_log)

            wandb.log(log)
            warmup.step()
            scheduler.step()


if __name__ == '__main__':
    main()
