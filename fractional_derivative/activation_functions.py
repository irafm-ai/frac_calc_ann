from typing import Callable
import torch
import torch.nn as nn
from torch.autograd import Function

MIN_FRAC_ORDER = 0.0
MAX_FRAC_ORDER = 2.0

def replace_activation_fc(module: nn.Module, new_act, old_act) -> int:
    """Recursively replace activation functions in a model.

    Args:
        module (nn.Module): Model or module.
        new_act: New activation function class, e. g. FracMish.
        old_act: Old activation class that is being replaced, e. g. nn.ReLU.

    Returns:
        int: Number of replaced activation functions.
    """
    counter = 0
    for attr_str in dir(module):
        target_attr = getattr(module, attr_str)
        if type(target_attr) == old_act:
            counter += 1
            setattr(module, attr_str, new_act())

    for _, im_child_module in module.named_children():
        counter += replace_activation_fc(im_child_module, new_act, old_act)

    return counter


def push_from_integer(x: torch.Tensor, t: float) -> torch.Tensor:
    """Pushes argument vector values from 0 and negative integers.

    Tested example:
    t = 0.1
     in: [-1.0, -0.99, -0.01, 0.01]
    out: [-1.1, -0.90, -0.10, 0.10]

    Args:
        x (torch.Tensor): Input argument.
        t (float): Distance minimum allowed distance from integer.

    Returns:
        torch.Tensor: x like tensor with pushed values.
    """
    int_x = torch.round(x)
    d = x - int_x
    if torch.all(torch.abs(d) > t):
        return x
    x = torch.where(torch.logical_and(-t < d, d < 0), int_x - t, x)
    x = torch.where(torch.logical_and(t > d, d >= 0), int_x + t, x)
    return x


def gamma(x: torch.Tensor, _min: float = -120.0, _max: float = 120.0):
    eps = torch.tensor(1e-4)
    if x > 0:
        x = torch.exp(torch.lgamma(x))
        x = torch.clamp(x, 0.0, _max)
        return x

    # avoid division by zero
    if torch.abs(bot := (torch.sin(torch.pi * x) * gamma(1 - x))) < eps:
        bot = eps
    x = torch.pi / bot
    x = torch.clamp(x, _min, _max)
    return x


def leaky_linear(x: torch.Tensor, _min: float, _max: float, slope: float) -> torch.Tensor:
    x = torch.where(x < _min, _min + (x - _min) * slope, x)
    x = torch.where(x > _max, _max + (x - _max) * slope, x)
    return x


class TrainableAct(nn.Module):
    pass


class BaseFracAct(TrainableAct):
    __constants__ = ['num_parameters', '_h', '_init', '_gamma_consts']
    num_parameters: int
    pool = []

    def __init__(self, iter: int, h: float) -> None:
        super().__init__()
        self.num_parameters = 1
        self._iter = iter
        self._h = h
        self.init = (
            torch.rand(1) * (MAX_FRAC_ORDER - MIN_FRAC_ORDER) + MIN_FRAC_ORDER
        )  # init [0, 2]
        self.frac_order = nn.Parameter(self.init, requires_grad=True)
        self.register_buffer(
            '_gamma_consts',
            torch.tensor([gamma(torch.tensor(el, dtype=torch.float)) for el in range(1, iter + 2)]),
        )

    def clip_params(self, _min: float, _max: float):
        # in-place version of clamp!
        self.frac_order.data.clamp_(min=_min, max=_max)


class FracSwish(BaseFracAct):
    """Published in paper: Fractional Adaptive Linear Units. Works ONLY for a \in [0, 1]"""

    def __init__(self, iter: int = 3, h: float = 0.5, **kwargs):
        super().__init__(iter, h)
        super().pool.append(self)

        self.init = torch.rand()
        self.frac_order = nn.Parameter(self.init, requires_grad=True)

    def forward(self, x):
        return x * torch.sigmoid(x) + self.frac_order * torch.sigmoid(x) * (
            1 - x * torch.sigmoid(x)
        )


class FracMish(BaseFracAct):
    def __init__(self, iter: int = 3, h: float = 0.5, **kwargs):
        super().__init__(iter, h)
        super().pool.append(self)

    def forward(self, x):
        sum = self.__sum(x)
        return sum

    def __sum(self, x):
        gamma_frac_one = gamma(1 + self.frac_order)

        acum = torch.zeros_like(x)

        term_2 = torch.square(torch.exp(x) + 1)
        acum += x * ((term_2 - 1) / (term_2 + 1))

        for i in range(1, self._iter):
            x_minus_nh = x - i * self._h
            term_1 = (gamma_frac_one * x_minus_nh) / (
                self._gamma_consts[i] * gamma(1 - i + self.frac_order)
            )
            term_2 = torch.square(torch.exp(x_minus_nh) + 1)
            acum += term_1 * ((term_2 - 1) / (term_2 + 1)) * torch.pow(torch.tensor(-1.0), i)

        acum *= 1 / torch.pow(self._h, self.frac_order)
        return acum


class FracGelu(BaseFracAct):
    def __init__(self, iter: int = 3, h: float = 0.5, **kwargs):
        super().__init__(iter, h)
        super().pool.append(self)

    def forward(self, x):
        sum = self.__sum(x)
        return sum

    def __sum(self, x):
        frac_order = self.frac_order
        sqrt_pi = 0.7978845608028654  # sqrt(2/pi)
        gamma_frac_one = gamma(1 + frac_order)

        acum = torch.zeros_like(x)

        term_1 = x
        term_2 = 1.0 + torch.tanh(sqrt_pi * (x + 0.044715 * x**3))
        acum += term_1 * term_2

        for i in range(1, self._iter):
            term_1 = (gamma_frac_one * (x - i * self._h)) / (
                self._gamma_consts[i] * gamma(1 - i + frac_order)
            )
            term_2 = 1.0 + torch.tanh(
                sqrt_pi * ((x - i * self._h) + 0.044715 * (x - i * self._h) ** 3)
            )
            acum += (term_1 * term_2) * torch.pow(torch.tensor(-1.0), i)

        acum *= 1 / (torch.pow(self._h, frac_order) * 2.0)

        return acum


class FALU(BaseFracAct):

    def __init__(self, iter: int = 3, h: float = 0.5, **kwargs):
        super().__init__(iter, h)
        super().pool.append(self)
        # frac_order part of the name to identify it for weight decay switch off
        self.frac_order_beta = nn.Parameter(torch.tensor(1.0), requires_grad=True)

    def clip_params(self, _min: float, _max: float):
        self.frac_order.data.clamp_(min=_min, max=_max)
        self.frac_order_beta.data.clamp_(1.0, 10.0)

    def forward(self, x):
        if self.frac_order <= 1:
            return FALU.a01(x, self.frac_order_beta, self.frac_order)
        else:
            return FALU.a12(x, self.frac_order_beta, self.frac_order)

    @staticmethod
    def a01(x, beta, frac_order):
        sig = torch.sigmoid(beta * x)
        g = x * sig
        return g + frac_order * sig * (1 - g)

    @staticmethod
    def a12(x, beta, frac_order):
        sig = torch.sigmoid(beta * x)
        g = x * sig
        h = g + torch.sigmoid(x) * (1 - g)

        return h + (frac_order - 1.0) * sig * (1 - 2 * h)


class FracSig(BaseFracAct):
    def __init__(self, iter: int = 3, h: float = 0.5, **kwargs):
        super().__init__(iter, h)
        super().pool.append(self)

    def forward(self, x):
        sum = self.__sum(x)
        return sum

    def __sum(self, x):
        acum = torch.zeros_like(x)

        one_frac = 1 + self.frac_order
        top = gamma(1 + self.frac_order)

        for i in range(self._iter):
            bot = self._gamma_consts[i] * gamma(one_frac - i) * (1 + torch.exp(-x + i * self._h))
            acum += (top / bot) * torch.pow(torch.tensor(-1.0), i)

        acum *= 1 / torch.pow(self._h, self.frac_order)

        return acum


class InterSig(BaseFracAct):
    def __init__(self, iter: int = 3, h: float = 0.5, **kwargs):
        super().__init__(iter, h)
        super().pool.append(self)

    def forward(self, x):
        if 0.0 <= self.frac_order < 1.0:
            tmp = torch.exp(-x)
            return (1 - self.frac_order) * torch.sigmoid(x) + self.frac_order * tmp / (1.0 + tmp) ** 2.0
        elif 1.0 <= self.frac_order <= 2.0:
            tmp = torch.exp(-x)
            return (2 - self.frac_order) * tmp / (1.0 + tmp) ** 2.0 + (self.frac_order - 1) * (tmp * (tmp - 1.0)) / (tmp + 1.0) ** 3 * -1.0


class FracSigAutoGrad(Function):
    @staticmethod
    def forward(ctx, x, frac_order, h, gamma_consts):
        acum = torch.zeros_like(x)

        one_frac = 1 + frac_order
        top = gamma(1 + frac_order)
        exp_minus_x = torch.exp(-x)

        bot = gamma_consts[0] * gamma(one_frac - 0.0) * (1 + exp_minus_x)
        acum += top / bot

        bot = gamma_consts[1] * gamma(one_frac - 1) * (1 + torch.exp(-x + 1 * h))
        acum -= top / bot

        bot = gamma_consts[2] * gamma(one_frac - 2) * (1 + torch.exp(-x + 2 * h))
        acum += top / bot

        acum *= 1 / torch.pow(h, frac_order)

        ctx.save_for_backward(frac_order, h, exp_minus_x)

        return acum

    @staticmethod
    def backward(ctx, grad):
        frac_order, h, exp_minus_x = ctx.saved_tensors

        # dout / dx
        acum_x = torch.zeros_like(exp_minus_x)
        gamma_a_plus_one = gamma(frac_order + 1)

        tmp = torch.exp(2 * h) * exp_minus_x
        top = gamma_a_plus_one * tmp
        bot = 2 * gamma(frac_order - 1) * torch.pow(tmp + 1, 2)
        acum_x += top / bot

        tmp = torch.exp(h) * exp_minus_x
        top = gamma_a_plus_one * tmp
        bot = gamma(frac_order) * torch.pow(tmp + 1, 2)
        acum_x -= top / bot

        top = exp_minus_x
        bot = torch.pow(exp_minus_x + 1, 2)
        acum_x += top / bot

        acum_x *= torch.pow(h, frac_order)

        # dout / da
        acum_a = torch.zeros_like(exp_minus_x)
        exp_plus_x = 1.0 / exp_minus_x  # torch.pow(exp_minus_x, -1)
        a_log_h = frac_order * torch.log(h)

        acum_a += (2 * (a_log_h - 1)) / (torch.exp(h) + exp_plus_x)
        acum_a += (-(frac_order - 1) * a_log_h + 2 * frac_order - 1) / (
            torch.exp(2 * h) + exp_plus_x
        )
        acum_a -= (2 * torch.log(h)) / (exp_plus_x + 1)
        acum_a *= 0.5 * exp_plus_x * torch.pow(h, -frac_order)

        return acum_x * grad, acum_a * grad, None, None


class FracReLU(BaseFracAct):
    def __init__(self, iter: int, h: float = 0.5, **kwargs):
        super().__init__(iter, h)
        super().pool.append(self)

    def forward(self, x):
        denominator = gamma(2 - self.frac_order)
        return torch.where(x >= 0, (1.0 / denominator) * (x ** (1 - self.frac_order)), 0)


# Wrappers that discards extra arguments, such as inplace which these classes do not have.
class Sigmoid(nn.Sigmoid):
    def __init__(self, **kwargs) -> None:
        super().__init__()


class PReLU(nn.PReLU):
    def __init__(
        self, num_parameters: int = 1, init: float = 0.25, device=None, dtype=None, **kwargs
    ) -> None:
        super().__init__(num_parameters, init, device, dtype)


class GELU(nn.GELU):
    def __init__(self, approximate: str = 'none', **kwargs) -> None:
        super().__init__(approximate)
