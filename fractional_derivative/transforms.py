from typing import Optional

import torchvision.transforms.functional as F
from PIL import Image
from torchvision import transforms


class Resize(transforms.Resize):
    """
    Resize transform with additional option to resize to the largest dimension.

    This class extends `transforms.Resize` to include an option to resize the image
    such that the largest dimension is set to the specified size. This is useful for
    maintaining the aspect ratio when resizing images.

    Parameters
    ----------
    size : int or tuple
        Desired output size. If `size` is an int, the smaller edge of the image will
        be matched to this number. If `size` is a tuple, it should be (height, width).
    largest : bool, optional
        If True, resize the image so that the largest dimension is set to `size`.
        Default is False.
    **kwargs : additional keyword arguments
        Additional arguments passed to `transforms.Resize`.

    Attributes
    ----------
    largest : bool
        Indicates whether to resize the image so that the largest dimension is `size`.
    """

    def __init__(self, size, largest=False, **kwargs):
        super().__init__(size, **kwargs)
        self.largest = largest

    @staticmethod
    def target_size(w: int, h: int, size: int, largest: Optional[bool] = False):
        """
        Compute the target size for resizing.

        Calculates the target dimensions for the image based on the original size and the
        desired output size, considering whether to resize based on the largest dimension.

        Parameters
        ----------
        w : int
            Width of the original image.
        h : int
            Height of the original image.
        size : int
            Desired output size.
        largest : bool, optional
            If True, resize based on the largest dimension. Default is False.

        Returns
        -------
        tuple
            A tuple containing the target height and width.
        """
        if h < w and largest:
            w, h = size, int(size * h / w)
        else:
            w, h = int(size * w / h), size
        size = (h, w)
        return size

    def __call__(self, img: Image):
        """
        Apply the resize transform to the input image.

        Resizes the input image to the target size calculated based on the specified size
        and whether to resize based on the largest dimension.

        Parameters
        ----------
        img : PIL.Image
            Input image to be resized.

        Returns
        -------
        PIL.Image
            The resized image.
        """
        size = self.size
        w, h = img.size
        target_size = self.target_size(w, h, size, self.largest)
        return F.resize(img, target_size, self.interpolation)

    def __repr__(self):
        r = super().__repr__()
        return r[:-1] + ", largest={})".format(self.largest)
