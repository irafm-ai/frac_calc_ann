import os
from typing import Any, Tuple

from PIL import Image
from torchvision.datasets import Caltech256


class RGBCaltech256(Caltech256):
    """
    Caltech256 dataset class for RGB images.

    This class is a subclass of `Caltech256` and ensures that images are
    converted to RGB format.

    Methods
    -------
    __getitem__(index: int) -> Tuple[Any, Any]
        Returns the image and target class index at the specified index.
    """

    def __getitem__(self, index: int) -> Tuple[Any, Any]:
        """
        Retrieve the RGB image and target class index at the specified index.

        Parameters
        ----------
        index : int
            Index of the item to retrieve.

        Returns
        -------
        tuple
            A tuple containing:
            - image (Image.Image): The RGB image.
            - target (int): The index of the target class.
        """

        img = Image.open(
            os.path.join(
                self.root,
                "256_ObjectCategories",
                self.categories[self.y[index]],
                f"{self.y[index] + 1:03d}_{self.index[index]:04d}.jpg",
            )
        )
        # Some of the Caltech images are grayscale with 1 channel
        img = img.convert("RGB")

        target = self.y[index]

        if self.transform is not None:
            img = self.transform(img)

        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target
