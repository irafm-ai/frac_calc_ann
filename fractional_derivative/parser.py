import torch.nn as nn
import timm

from . import (activation_functions, efficientnet, losses, resnet)


def get_activation_fc(name):
    """From cmd argument to class reference"""
    activation_fcs = {
        'relu': nn.ReLU,
        'swish': nn.SiLU,
        'mish': nn.Mish,
        'sig': activation_functions.Sigmoid,
        'selu': nn.SELU,
        'elu': nn.ELU,
        'prelu': activation_functions.PReLU,
        'leaky_relu': nn.LeakyReLU,
        'gelu': activation_functions.GELU,
        'falu': activation_functions.FALU,
        'frac_swish': activation_functions.FracSwish,
        'frac_mish': activation_functions.FracMish,
        'frac_sig': activation_functions.FracSig,
        'frac_gelu': activation_functions.FracGelu,
        'inter_sig': activation_functions.InterSig,
    }
    return activation_fcs[name]


def get_model(name, activation_fc, num_classes) -> nn.Module:
    """From cmd argument to class reference"""
    if 'resnet' in name:
        return getattr(resnet, name)(activation_fc, num_classes)

    elif 'effnet' in name:
        # can you untangle this? :)
        models = {f'effnet{el}': getattr(efficientnet, f'efficientnet_{el}') for el in [f'b{i}' for i in range(8)]}
        return models[name](activation_fc=activation_fc, num_classes=num_classes)

    # Grab model from timm and recursively replace its activation functions with ours
    elif 'mobilenetv4' in name:
        model = timm.create_model(name, num_classes=num_classes)
        counter = activation_functions.replace_activation_fc(model, activation_fc, nn.ReLU)
        assert counter > 0, 'Number of replaced functions is {counter} < 0'
        return model

    elif 'convnext' in name:
        model = timm.create_model(name, num_classes=num_classes, use_grn=False)
        counter = activation_functions.replace_activation_fc(model, activation_fc, timm.layers.activations.GELU)
        assert counter > 0, 'Number of replaced functions is {counter} < 0'
        return model

    elif 'eva' in name:

        model = timm.create_model(name, num_classes=num_classes)
        counter = activation_functions.replace_activation_fc(model, activation_fc, nn.modules.activation.SiLU)
        assert counter > 0, 'Number of replaced functions is {counter} < 0'
        return model

    else:
        raise ValueError()



def get_loss_fc(name, frac_order=None):
    """From cmd argument to class reference"""
    loss_fcs = {
        'mse': nn.MSELoss(),
        'frac_mse': losses.FracMSE(frac_order=frac_order),
        'huber': nn.HuberLoss(),
        'frac_huber': losses.FracHuber(frac_order=frac_order),
    }
    return loss_fcs[name]
