import logging
import os
import random
from typing import Dict, Iterator, List, Tuple

import numpy as np
import torch

from . import activation_functions, parser, resnet


def get_logger(name: str, logger_level=logging.DEBUG, stream_level=logging.DEBUG):
    logger = logging.getLogger(name)
    logger.setLevel(logger_level)

    FORMATTER = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    # logging into console
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(FORMATTER)
    stream_handler.setLevel(stream_level)
    logger.addHandler(stream_handler)

    return logger


def set_seed(seed: int = 42):
    """
    Set the random seed for reproducibility across various libraries.

    Parameters
    ----------
    seed : int, optional
        The seed value to use for random number generators, by default 42.
    """
    # https://pytorch.org/docs/stable/notes/randomness.html
    random.seed(seed)
    np.random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    torch.use_deterministic_algorithms(mode=True)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)


def seed_worker(worker_id: int):
    """
    Seed function for dataloader workers to ensure reproducibility.

    Parameters
    ----------
    worker_id : int
        The worker ID assigned by the DataLoader.
    """
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


def disable_weight_decay_for_frac_af(
    named_parameters: Iterator[Tuple[str, torch.nn.Parameter]], weight_decay: float
) -> List[Dict]:
    """
    Disable weight decay for fractional activation function parameters in an optimizer.

    Parameters
    ----------
    named_parameters : Iterator[Tuple[str, torch.nn.Parameter]]
        An iterator of named model parameters.
    weight_decay : float
        The weight decay value to apply to non-fractional parameters.

    Returns
    -------
    List[Dict]
        A list of dictionaries with grouped parameters for the optimizer,
        where weight decay is set to the specified value for non-fractional parameters
        and to 0.0 for fractional parameters.
    """
    # https://github.com/huggingface/transformers/blob/7c6cd0ac28f1b760ccb4d6e4761f13185d05d90b/src/transformers/trainer.py#L800
    named_parameters = list(named_parameters)
    # get only fractional orders and falu beta
    frac_parameters = [name for name, _ in named_parameters if "frac_order" in name or "beta" in name]
    optimizer_grouped_parameters = [
        {
            "params": [p for n, p in named_parameters if n not in frac_parameters],
            "weight_decay": weight_decay,
        },
        {
            "params": [p for n, p in named_parameters if n in frac_parameters],
            "weight_decay": 0.0,  # 0 decay for fractional orders and betas
        },
    ]
    return optimizer_grouped_parameters
