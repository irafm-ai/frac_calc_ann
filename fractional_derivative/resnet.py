"""
This module is derived from the original ResNet implementation provided 
by https://github.com/nouhautayomi/resnet-cifar-pytorch. It has been modified
to include support for custom activation functions, including fractional 
activation functions, as arguments to the ResNet model.

Modifications:
- Added functionality to pass activation functions as arguments.
- Supported integration of fractional activation functions.
- Added ResNets for CIFAR-10 and ImageNet-1K
"""

from typing import Callable

import torch
import torch.nn as nn


def conv3x3(in_planes: int, out_planes: int, stride: int) -> nn.Module:
    """
    Creates a 3x3 convolutional layer with specified input and output channels and stride.

    Parameters
    ----------
    in_planes : int
        Number of input channels.
    out_planes : int
        Number of output channels.
    stride : int
        Stride of the convolution.

    Returns
    -------
    nn.Module
        A 3x3 convolutional layer.
    """
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)


class BasicBlock(nn.Module):
    """
    Basic building block for ResNet, consisting of two 3x3 convolutions with batch normalization
    and activation functions.

    Parameters
    ----------
    activation_fc : Callable
        Callable that returns the activation function module.
    in_planes : int
        Number of input channels.
    planes : int
        Number of output channels.
    stride : int
        Stride of the first convolution.

    Attributes
    ----------
    expansion : int
        The expansion factor for the block (set to 1 for BasicBlock).
    _act_fn_pool : nn.ModuleList
        List of activation functions applied in the block.
    conv1 : nn.Module
        First 3x3 convolutional layer.
    conv2 : nn.Module
        Second 3x3 convolutional layer.
    bn1 : nn.BatchNorm2d
        Batch normalization layer after the first convolution.
    bn2 : nn.BatchNorm2d
        Batch normalization layer after the second convolution.
    shortcut : nn.Sequential
        Shortcut connection, used if input and output dimensions differ.

    Methods
    -------
    forward(x: torch.Tensor) -> torch.Tensor
        Forward pass through the block.
    """

    expansion = 1

    def __init__(self, activation_fc: Callable, in_planes: int, planes: int, stride: int):
        super(BasicBlock, self).__init__()
        self._act_fn_pool = nn.ModuleList(
            [
                activation_fc(),
                activation_fc(),
            ]
        )
        self.conv1 = conv3x3(in_planes, planes, stride)
        self.conv2 = conv3x3(planes, planes, 1)
        self.bn1 = nn.BatchNorm2d(planes)
        self.bn2 = nn.BatchNorm2d(planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion * planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion * planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion * planes),
            )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        out = self._act_fn_pool[0](self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        out = self._act_fn_pool[1](out)
        return out


class Bottleneck(nn.Module):
    """
    Bottleneck building block for ResNet, consisting of three convolutions with batch normalization
    and activation functions.

    Parameters
    ----------
    activation_fc : Callable
        Callable that returns the activation function module.
    in_planes : int
        Number of input channels.
    planes : int
        Number of output channels.
    stride : int
        Stride of the second convolution.

    Attributes
    ----------
    expansion : int
        The expansion factor for the block (set to 4 for Bottleneck).
    _act_fn_pool : nn.ModuleList
        List of activation functions applied in the block.
    conv1 : nn.Module
        First 1x1 convolutional layer.
    conv2 : nn.Module
        Second 3x3 convolutional layer.
    conv3 : nn.Module
        Third 1x1 convolutional layer.
    bn1 : nn.BatchNorm2d
        Batch normalization layer after the first convolution.
    bn2 : nn.BatchNorm2d
        Batch normalization layer after the second convolution.
    bn3 : nn.BatchNorm2d
        Batch normalization layer after the third convolution.
    shortcut : nn.Sequential
        Shortcut connection, used if input and output dimensions differ.

    Methods
    -------
    forward(x: torch.Tensor) -> torch.Tensor
        Forward pass through the block.
    """

    expansion = 4

    def __init__(self, activation_fc: Callable, in_planes: int, planes: int, stride: int):
        super(Bottleneck, self).__init__()
        self._act_fn_pool = nn.ModuleList([activation_fc() for _ in range(3)])
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, bias=False)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.conv3 = nn.Conv2d(planes, self.expansion * planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.bn3 = nn.BatchNorm2d(self.expansion * planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion * planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion * planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion * planes),
            )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        out = self._act_fn_pool[0](self.bn1(self.conv1(x)))
        out = self._act_fn_pool[1](self.bn2(self.conv2(out)))
        out = self.bn3(self.conv3(out))
        out += self.shortcut(x)
        out = self._act_fn_pool[2](out)
        return out


class ResNetImageNet(nn.Module):
    """
    ResNet model adapted for ImageNet dataset. Uses a sequence of ResNet blocks with increasing depth.

    Parameters
    ----------
    activation_fc : Callable
        Callable that returns the activation function module.
    block : Callable
        Callable that returns the block type (BasicBlock or Bottleneck).
    num_blocks : int
        List of integers specifying the number of blocks in each layer.
    num_classes : int
        Number of output classes.

    Attributes
    ----------
    act_fn_pool : nn.ModuleList
        List of activation functions used throughout the network.
    in_planes : int
        Number of input channels to the first layer.
    conv1 : nn.Conv2d
        Initial convolutional layer.
    bn1 : nn.BatchNorm2d
        Batch normalization layer after the initial convolution.
    layer1 : nn.Sequential
        First sequence of blocks.
    layer2 : nn.Sequential
        Second sequence of blocks.
    layer3 : nn.Sequential
        Third sequence of blocks.
    layer4 : nn.Sequential
        Fourth sequence of blocks.
    avg_pool : nn.AdaptiveAvgPool2d
        Adaptive average pooling layer.
    linear : nn.Linear
        Final linear layer for classification.

    Methods
    -------
    _make_layer(activation_fc: Callable, block: Callable, planes: int, num_blocks: int, stride: int) -> nn.Sequential
        Creates a sequence of blocks for a specific layer.
    forward(x: torch.Tensor) -> torch.Tensor
        Forward pass through the network.
    """

    def __init__(self, activation_fc: Callable, block: Callable, num_blocks: int, num_classes: int):
        super(ResNetImageNet, self).__init__()
        self.act_fn_pool = nn.ModuleList()
        self.in_planes = 64
        self.act_fn_pool.append(activation_fc())

        # for imagenet -> bigger input res
        self.conv1 = nn.Conv2d(3, self.in_planes, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = nn.BatchNorm2d(self.in_planes)
        self.layer1 = self._make_layer(activation_fc, block, 64, num_blocks[0], 1)
        self.layer2 = self._make_layer(activation_fc, block, 128, num_blocks[1], 2)
        self.layer3 = self._make_layer(activation_fc, block, 256, num_blocks[2], 2)
        self.layer4 = self._make_layer(activation_fc, block, 512, num_blocks[3], 2)
        self.avg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.linear = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_layer(
        self, activation_fc: Callable, block: Callable, planes: int, num_blocks: int, stride: int
    ) -> nn.Sequential:
        """
        Creates a sequential container of ResNet blocks for a specific layer.

        Constructs a sequence of blocks with a specified number of blocks and stride,
        and updates the input channels for the next layer. Each block is instantiated
        using the provided block type and activation function.

        Parameters
        ----------
        activation_fc : Callable
            Callable that returns the activation function module to be used in the blocks.
        block : Callable
            Callable that returns the block type (e.g., BasicBlock) to be used in the layer.
        planes : int
            Number of output channels for the blocks in this layer.
        num_blocks : int
            Number of blocks to include in this layer.
        stride : int
            Stride for the first block in this layer.

        Returns
        -------
        nn.Sequential
            A sequential container of the constructed blocks for the layer.
        """
        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        for stride in strides:
            layers.append(block(activation_fc, self.in_planes, planes, stride))
            self.act_fn_pool.extend(layers[-1]._act_fn_pool)
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        out = self.act_fn_pool[0](self.bn1(self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = self.avg_pool(out)
        out = torch.flatten(out, 1)
        out = self.linear(out)
        return out


class ResNetCIFAR10(nn.Module):
    """
    ResNet model adapted for CIFAR-10 dataset. Uses a sequence of ResNet blocks with decreasing depth.

    Parameters
    ----------
    activation_fc : Callable
        Callable that returns the activation function module.
    block : Callable
        Callable that returns the block type (BasicBlock).
    num_blocks : int
        List of integers specifying the number of blocks in each layer.
    num_classes : int
        Number of output classes.

    Attributes
    ----------
    act_fn_pool : nn.ModuleList
        List of activation functions used throughout the network.
    in_planes : int
        Number of input channels to the first layer.
    conv1 : nn.Conv2d
        Initial convolutional layer.
    bn1 : nn.BatchNorm2d
        Batch normalization layer after the initial convolution.
    layer1 : nn.Sequential
        First sequence of blocks.
    layer2 : nn.Sequential
        Second sequence of blocks.
    layer3 : nn.Sequential
        Third sequence of blocks.
    avg_pool : nn.AdaptiveAvgPool2d
        Adaptive average pooling layer.
    linear : nn.Linear
        Final linear layer for classification.

    Methods
    -------
    _make_layer(activation_fc: Callable, block: Callable, planes: int, num_blocks: int, stride: int) -> nn.Sequential
        Creates a sequence of blocks for a specific layer.
    forward(x: torch.Tensor) -> torch.Tensor
        Forward pass through the network.
    """

    def __init__(self, activation_fc: Callable, block: Callable, num_blocks: int, num_classes: int):
        super(ResNetCIFAR10, self).__init__()
        self.act_fn_pool = nn.ModuleList()
        self.in_planes = 16
        self.act_fn_pool.append(activation_fc())

        self.conv1 = conv3x3(3, self.in_planes, 1)
        self.bn1 = nn.BatchNorm2d(self.in_planes)
        self.layer1 = self._make_layer(activation_fc, block, 16, num_blocks[0], 1)
        self.layer2 = self._make_layer(activation_fc, block, 32, num_blocks[1], 2)
        self.layer3 = self._make_layer(activation_fc, block, 64, num_blocks[2], 2)
        self.avg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.linear = nn.Linear(64 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Linear) or isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def _make_layer(
        self, activation_fc: Callable, block: Callable, planes: int, num_blocks: int, stride: int
    ) -> nn.Sequential:
        """
        Creates a sequential container of ResNet blocks for a specific layer.

        Constructs a sequence of blocks with a specified number of blocks and stride,
        and updates the input channels for the next layer. Each block is instantiated
        using the provided block type and activation function.

        Parameters
        ----------
        activation_fc : Callable
            Callable that returns the activation function module to be used in the blocks.
        block : Callable
            Callable that returns the block type (e.g., BasicBlock) to be used in the layer.
        planes : int
            Number of output channels for the blocks in this layer.
        num_blocks : int
            Number of blocks to include in this layer.
        stride : int
            Stride for the first block in this layer.

        Returns
        -------
        nn.Sequential
            A sequential container of the constructed blocks for the layer.
        """
        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        for stride in strides:
            layers.append(block(activation_fc, self.in_planes, planes, stride))
            self.act_fn_pool.extend(layers[-1]._act_fn_pool)
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        out = self.act_fn_pool[0](self.bn1(self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.avg_pool(out)
        out = torch.flatten(out, 1)
        out = self.linear(out)
        return out


def resnet18(activation_fc: Callable, num_classes=10) -> ResNetImageNet:
    return ResNetImageNet(activation_fc, BasicBlock, [2, 2, 2, 2], num_classes)


def resnet18a(activation_fc: Callable, num_classes: int = 10) -> ResNetCIFAR10:
    return ResNetCIFAR10(activation_fc, BasicBlock, [2, 2, 2], num_classes)


def resnet20(activation_fc: Callable, num_classes: int = 10) -> ResNetCIFAR10:
    return ResNetCIFAR10(activation_fc, BasicBlock, [3, 3, 3], num_classes)


def resnet32(activation_fc: Callable, num_classes: int = 10) -> ResNetCIFAR10:
    return ResNetCIFAR10(activation_fc, BasicBlock, [5, 5, 5], num_classes)


def resnet34(activation_fc: Callable, num_classes: int = 10) -> ResNetImageNet:
    return ResNetImageNet(activation_fc, BasicBlock, [3, 4, 6, 3], num_classes)


def resnet44(activation_fc: Callable, num_classes: int = 10) -> ResNetCIFAR10:
    return ResNetCIFAR10(activation_fc, BasicBlock, [7, 7, 7], num_classes)


def resnet50(activation_fc: Callable, num_classes: int = 10) -> ResNetImageNet:
    return ResNetImageNet(activation_fc, Bottleneck, [3, 4, 6, 3], num_classes)


def resnet56(activation_fc: Callable, num_classes: int = 10) -> ResNetCIFAR10:
    return ResNetCIFAR10(activation_fc, BasicBlock, [9, 9, 9], num_classes)


def resnet101(activation_fc: Callable, num_classes: int = 10) -> ResNetImageNet:
    return ResNetImageNet(activation_fc, Bottleneck, [3, 4, 23, 3], num_classes)


def resnet110(activation_fc: Callable, num_classes: int = 10) -> ResNetCIFAR10:
    return ResNetCIFAR10(activation_fc, BasicBlock, [18, 18, 18], num_classes)


def resnet152(activation_fc: Callable, num_classes: int = 10) -> ResNetImageNet:
    return ResNetImageNet(activation_fc, Bottleneck, [3, 8, 36, 3], num_classes)


def resnet1202(activation_fc: Callable, num_classes: int = 10) -> ResNetCIFAR10:
    return ResNetCIFAR10(activation_fc, BasicBlock, [200, 200, 200], num_classes)
