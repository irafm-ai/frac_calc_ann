apt-get update
apt-get install -y tmux
apt-get install -y nvtop

python3 -m pip install --upgrade pip
pip3 install --no-cache-dir -r requirements.txt

pre-commit install
